# Copyright 2007, 2009, 2010 Mike Kelly <pioto@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'bash-completion-20060301-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2006 Gentoo Foundation

require github [ user=scop release=${PV} suffix=tar.xz ]

export_exlib_phases pkg_pretend src_install

SUMMARY="Programmable Completion for bash"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES="
    build+run:
        !sys-apps/util-linux[<2.31] [[
            description = [ sys-apps/util-linux installs mount and umount completions previously provided by this package ]
            resolution = uninstall-blocked-after
        ]]
    run:
        app-shells/bash[>=3.2_p19]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --without-pytest
)

# scripts in the default /etc/profile.d/ aren't enabled for interactive non-login shells
DEFAULT_SRC_INSTALL_PARAMS=( "profiledir=/etc/bash/bashrc.d" )

bash-completion_pkg_pretend() {
    if [[ -f "${ROOT}"/etc/profile.d/bash_completion.sh ]] ; then
        ewarn "Script /etc/profile.d/bash_completion.sh has been moved to /etc/bash/bashrc.d/"
        ewarn "and can be safely removed after upgrade."
    fi
}

bash-completion_src_install() {
    default

    # don't install disabled completions
    edo rm "${IMAGE}"/usr/share/bash-completion/completions/_*

    edo rm "${IMAGE}"/usr/share/bash-completion/completions/xxd
}

