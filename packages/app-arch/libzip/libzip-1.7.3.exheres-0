# Copyright 2008 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2018-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="C Library for manipulating zip archives"
DESCRIPTION="
A C library for reading, creating, and modifying zip archives. Files can be added from data
buffers, files, or compressed data copied directly from other zip archives. Changes made without
closing the archive can be reverted. Decryption and encryption of Winzip AES and decryption of
legacy PKware encrypted files is supported. The API is documented by man pages.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/download/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    examples
    ( providers: gnutls libressl mbedtls openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-apps/groff
    build+run:
        app-arch/bzip2
        app-arch/xz
        sys-libs/zlib[>=1.1.2]
        providers:gnutls? (
            dev-libs/gnutls
            dev-libs/nettle:=
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:mbedtls? ( dev-libs/mbedtls )
        providers:openssl? ( dev-libs/openssl )
"

src_configure() {
    local cmakeparams=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DENABLE_BZIP2:BOOL=TRUE
        -DENABLE_LZMA:BOOL=TRUE
        -DENABLE_COMMONCRYPTO:BOOL=TRUE
    )

    cmakeparams+=(
        $(cmake_build EXAMPLES)
        $(cmake_enable providers:gnutls GNUTLS)
        $(cmake_enable providers:mbedtls MBEDTLS)
    )
    if option providers:openssl || option providers:libressl ; then
        cmakeparams+=( -DENABLE_OPENSSL:BOOL=TRUE )
    else
        cmakeparams+=( -DENABLE_OPENSSL:BOOL=FALSE )
    fi
    ecmake ${cmakeparams[@]}
}

