From c2e304681929fea79ce8e9c5a1e00cd2f293a72d Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Zbigniew=20J=C4=99drzejewski-Szmek?= <zbyszek@in.waw.pl>
Date: Thu, 16 Jan 2020 15:53:57 +0100
Subject: [PATCH 090/104] shared/sysctl-util: normalize repeated slashes or
 dots to a single value

We use those strings as hash keys. While writing "a...b" looks strange,
"a///b" does not look so strange. Both syntaxes would actually result in the
value being correctly written to the file, but they would confuse our
de-deplication over keys. So let's normalize. Output also becomes nicer.

Add test.

(cherry picked from commit f3b136a4847a0993e2dc1197779160dca4da6dac)
---
 src/shared/sysctl-util.c    | 32 ++++++++++++++++-----------
 src/test/meson.build        |  4 ++++
 src/test/test-sysctl-util.c | 44 +++++++++++++++++++++++++++++++++++++
 3 files changed, 67 insertions(+), 13 deletions(-)
 create mode 100644 src/test/test-sysctl-util.c

diff --git a/src/shared/sysctl-util.c b/src/shared/sysctl-util.c
index 12fb3ef7ea..8543dbd2d0 100644
--- a/src/shared/sysctl-util.c
+++ b/src/shared/sysctl-util.c
@@ -9,6 +9,7 @@
 #include "fileio.h"
 #include "log.h"
 #include "macro.h"
+#include "path-util.h"
 #include "string-util.h"
 #include "sysctl-util.h"
 
@@ -16,22 +17,27 @@ char *sysctl_normalize(char *s) {
         char *n;
 
         n = strpbrk(s, "/.");
+
         /* If the first separator is a slash, the path is
          * assumed to be normalized and slashes remain slashes
          * and dots remains dots. */
-        if (!n || *n == '/')
-                return s;
-
-        /* Otherwise, dots become slashes and slashes become
-         * dots. Fun. */
-        while (n) {
-                if (*n == '.')
-                        *n = '/';
-                else
-                        *n = '.';
-
-                n = strpbrk(n + 1, "/.");
-        }
+
+        if (n && *n == '.')
+                /* Dots become slashes and slashes become dots. Fun. */
+                do {
+                        if (*n == '.')
+                                *n = '/';
+                        else
+                                *n = '.';
+
+                        n = strpbrk(n + 1, "/.");
+                } while (n);
+
+        path_simplify(s, true);
+
+        /* Kill the leading slash, but keep the first character of the string in the same place. */
+        if (*s == '/' && *(s+1))
+                memmove(s, s+1, strlen(s));
 
         return s;
 }
diff --git a/src/test/meson.build b/src/test/meson.build
index 7e5a61a11e..dd844f0291 100644
--- a/src/test/meson.build
+++ b/src/test/meson.build
@@ -325,6 +325,10 @@ tests += [
          [],
          []],
 
+        [['src/test/test-sysctl-util.c'],
+         [],
+         []],
+
         [['src/test/test-user-util.c'],
          [],
          []],
diff --git a/src/test/test-sysctl-util.c b/src/test/test-sysctl-util.c
new file mode 100644
index 0000000000..2b957dd4d6
--- /dev/null
+++ b/src/test/test-sysctl-util.c
@@ -0,0 +1,44 @@
+/* SPDX-License-Identifier: LGPL-2.1+ */
+
+#include "strv.h"
+#include "sysctl-util.h"
+#include "tests.h"
+
+static const char* cases[] = {
+        "a.b.c", "a/b/c",
+        "a/b/c", "a/b/c",
+        "a/b.c/d", "a/b.c/d",
+        "a.b/c.d", "a/b.c/d",
+
+        "net.ipv4.conf.enp3s0/200.forwarding", "net/ipv4/conf/enp3s0.200/forwarding",
+        "net/ipv4/conf/enp3s0.200/forwarding", "net/ipv4/conf/enp3s0.200/forwarding",
+
+        "a...b...c", "a/b/c",
+        "a///b///c", "a/b/c",
+        ".a...b...c", "a/b/c",
+        "/a///b///c", "a/b/c",
+        NULL,
+};
+
+static void test_sysctl_normalize(void) {
+        log_info("/* %s */", __func__);
+
+        const char **s, **expected;
+        STRV_FOREACH_PAIR(s, expected, cases) {
+                _cleanup_free_ char *t;
+
+                assert_se(t = strdup(*s));
+                assert_se(sysctl_normalize(t) == t);
+
+                log_info("\"%s\" → \"%s\", expected \"%s\"", *s, t, *expected);
+                assert_se(streq(t, *expected));
+        }
+}
+
+int main(int argc, char *argv[]) {
+        test_setup_logging(LOG_INFO);
+
+        test_sysctl_normalize();
+
+        return 0;
+}
-- 
2.25.1

