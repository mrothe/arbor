Upstream: yes
From cbc872a63f81ad9482bb288165af63d7444fa3ed Mon Sep 17 00:00:00 2001
From: Saleem Abdulrasool <compnerd@compnerd.org>
Date: Wed, 6 Nov 2019 10:51:42 -0800
Subject: [PATCH] unwind: disable RTTI during the build of libunwind

Disable the type information emission for libunwind.  libunwind does not
use `dynamic_cast`.  This results in a smaller binary, and more
importantly, avoids the dependency on libc++abi.  This ensures that we
have complete symbol resolution of symbols on ELF targets without
linking to the C++ runtime support library.  This change avoids the
emission of a reference to `__si_class_type_info`.
---
 libunwind/src/CMakeLists.txt | 10 ++++++++++
 1 file changed, 10 insertions(+)

diff --git a/libunwind/src/CMakeLists.txt b/libunwind/src/CMakeLists.txt
index ed20ff0015f4..2dab16afbdf9 100644
--- a/libunwind/src/CMakeLists.txt
+++ b/libunwind/src/CMakeLists.txt
@@ -111,6 +111,11 @@ set_property(SOURCE ${LIBUNWIND_C_SOURCES}
 # Build the shared library.
 if (LIBUNWIND_ENABLE_SHARED)
   add_library(unwind_shared SHARED ${LIBUNWIND_SOURCES} ${LIBUNWIND_HEADERS})
+  if(CMAKE_C_COMPILER_ID STREQUAL MSVC)
+    target_compile_options(unwind_shared PRIVATE /GR-)
+  else()
+    target_compile_options(unwind_shared PRIVATE -fno-rtti)
+  endif()
   if(COMMAND llvm_setup_rpath)
     llvm_setup_rpath(unwind_shared)
   endif()
@@ -142,6 +147,11 @@ endif()
 # Build the static library.
 if (LIBUNWIND_ENABLE_STATIC)
   add_library(unwind_static STATIC ${LIBUNWIND_SOURCES} ${LIBUNWIND_HEADERS})
+  if(CMAKE_C_COMPILER_ID STREQUAL MSVC)
+    target_compile_options(unwind_static PRIVATE /GR-)
+  else()
+    target_compile_options(unwind_static PRIVATE -fno-rtti)
+  endif()
   target_link_libraries(unwind_static PRIVATE ${LIBUNWIND_LIBRARIES})
   set_target_properties(unwind_static
                         PROPERTIES
