Upstream: yes, taken from release/2.31/master

From 9db2970506ca56732ae55f420c48f0c57ac6eb37 Mon Sep 17 00:00:00 2001
From: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date: Tue, 3 Mar 2020 14:59:13 -0300
Subject: [PATCH 09/44] linux: Clear mode_t padding bits (BZ#25623)

The kernel might not clear the padding value for the ipc_perm mode
fields in compat mode (32 bit running on a 64 bit kernel).  It was
fixed on v4.14 when the ipc compat code was refactored to move
(commits 553f770ef71b, 469391684626, c0ebccb6fa1e).

Although it is most likely a kernel issue, it was shown only due
BZ#18231 fix which made all the SysVIPC mode_t 32-bit regardless of
the kABI.

This patch fixes it by explicitly zeroing the upper bits for such
cases.  The __ASSUME_SYSVIPC_BROKEN_MODE_T case already handles
it with the shift.

(The aarch64 ipc_priv.h is superflous since
__ASSUME_SYSVIPC_DEFAULT_IPC_64 is now defined as default).

Checked on i686-linux-gnu on 3.10 and on 4.15 kernel.

(cherry picked from commit 82025bad80429c67a4d75f098155b5e02b5112b4)
---
 sysdeps/unix/sysv/linux/msgctl.c | 9 +++++++--
 sysdeps/unix/sysv/linux/semctl.c | 9 +++++++--
 sysdeps/unix/sysv/linux/shmctl.c | 9 +++++++--
 3 files changed, 21 insertions(+), 6 deletions(-)

diff --git a/sysdeps/unix/sysv/linux/msgctl.c b/sysdeps/unix/sysv/linux/msgctl.c
index 27879e76cd..eb28835b3a 100644
--- a/sysdeps/unix/sysv/linux/msgctl.c
+++ b/sysdeps/unix/sysv/linux/msgctl.c
@@ -61,7 +61,6 @@ __new_msgctl (int msqid, int cmd, struct msqid_ds *buf)
 
   int ret = msgctl_syscall (msqid, cmd, buf);
 
-#ifdef __ASSUME_SYSVIPC_BROKEN_MODE_T
   if (ret >= 0)
     {
       switch (cmd)
@@ -69,10 +68,16 @@ __new_msgctl (int msqid, int cmd, struct msqid_ds *buf)
 	case IPC_STAT:
 	case MSG_STAT:
 	case MSG_STAT_ANY:
+#ifdef __ASSUME_SYSVIPC_BROKEN_MODE_T
 	  buf->msg_perm.mode >>= 16;
+#else
+	  /* Old Linux kernel versions might not clear the mode padding.  */
+	  if (sizeof ((struct msqid_ds){0}.msg_perm.mode)
+	      != sizeof (__kernel_mode_t))
+	    buf->msg_perm.mode &= 0xFFFF;
+#endif
 	}
     }
-#endif
 
   return ret;
 }
diff --git a/sysdeps/unix/sysv/linux/semctl.c b/sysdeps/unix/sysv/linux/semctl.c
index 0c3eb0932f..0a79e8e4f5 100644
--- a/sysdeps/unix/sysv/linux/semctl.c
+++ b/sysdeps/unix/sysv/linux/semctl.c
@@ -92,7 +92,6 @@ __new_semctl (int semid, int semnum, int cmd, ...)
 
   int ret = semctl_syscall (semid, semnum, cmd, arg);
 
-#ifdef __ASSUME_SYSVIPC_BROKEN_MODE_T
   if (ret >= 0)
     {
       switch (cmd)
@@ -100,10 +99,16 @@ __new_semctl (int semid, int semnum, int cmd, ...)
         case IPC_STAT:
         case SEM_STAT:
         case SEM_STAT_ANY:
+#ifdef __ASSUME_SYSVIPC_BROKEN_MODE_T
           arg.buf->sem_perm.mode >>= 16;
+#else
+	  /* Old Linux kernel versions might not clear the mode padding.  */
+	  if (sizeof ((struct semid_ds){0}.sem_perm.mode)
+	      != sizeof (__kernel_mode_t))
+	    arg.buf->sem_perm.mode &= 0xFFFF;
+#endif
 	}
     }
-#endif
 
   return ret;
 }
diff --git a/sysdeps/unix/sysv/linux/shmctl.c b/sysdeps/unix/sysv/linux/shmctl.c
index 39fa861e17..aed9e5260e 100644
--- a/sysdeps/unix/sysv/linux/shmctl.c
+++ b/sysdeps/unix/sysv/linux/shmctl.c
@@ -63,7 +63,6 @@ __new_shmctl (int shmid, int cmd, struct shmid_ds *buf)
 
   int ret = shmctl_syscall (shmid, cmd, buf);
 
-#ifdef __ASSUME_SYSVIPC_BROKEN_MODE_T
   if (ret >= 0)
     {
       switch (cmd)
@@ -71,10 +70,16 @@ __new_shmctl (int shmid, int cmd, struct shmid_ds *buf)
         case IPC_STAT:
         case SHM_STAT:
         case SHM_STAT_ANY:
+#ifdef __ASSUME_SYSVIPC_BROKEN_MODE_T
           buf->shm_perm.mode >>= 16;
+#else
+	  /* Old Linux kernel versions might not clear the mode padding.  */
+	  if (sizeof ((struct shmid_ds){0}.shm_perm.mode)
+	      != sizeof (__kernel_mode_t))
+	    buf->shm_perm.mode &= 0xFFFF;
+#endif
 	}
     }
-#endif
 
   return ret;
 }
-- 
2.27.0

