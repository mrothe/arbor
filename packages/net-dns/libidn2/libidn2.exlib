# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ pn=libidn pnv=${PNV} suffix=tar.gz ] \
    alternatives

export_exlib_phases src_install

SUMMARY="Free software implementation of IDNA2008"
HOMEPAGE="https://www.gnu.org/software/libidn/#${PN}"

LICENCES="
    || ( GPL-2 LGPL-3 ) [[ note = [ library ] ]]
    GPL-3 [[ note = [ tools, tests, examples, and auxilliary files ] ]]
"
MYOPTIONS="
    doc
    gtk-doc
    ( linguas: cs da de eo es fi fr fur hr hu id it ja nl pl pt_BR ro sr sv uk vi zh_CN )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.3]
        virtual/pkg-config
        doc? (
            sys-apps/ronn
            sys-apps/texinfo
        )
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/libunistring
    run:
        !net-dns/libidn2:0[<2.0.5-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
    --disable-valgrind-tests
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    doc
    gtk-doc
)

libidn2_src_install() {
    default

    local arch_dependent_alternatives=(
        /usr/$(exhost --target)/bin/idn2               idn2-${SLOT}
        /usr/$(exhost --target)/include/idn2.h         idn2.h-${SLOT}
        /usr/$(exhost --target)/lib/libidn2.la         ${PN}-${SLOT}.la
        /usr/$(exhost --target)/lib/libidn2.so         ${PN}-${SLOT}.so
        /usr/$(exhost --target)/lib/libidn2.so.0       ${PN}-${SLOT}.so.0
        /usr/$(exhost --target)/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if [[ -n "${LINGUAS}" ]] ; then
        for file in "${IMAGE}"/usr/share/locale/*/LC_MESSAGES/*; do
            local arch_independent_alternatives=(
                ${file#${IMAGE}}
                ${file#${IMAGE}}-${SLOT}
            )
        done
        if ever at_least 2.2.0 ; then
            # Upstream decided to downgrade SONAME back to 0 so we raise importance so SLOT 0 will
            # take precedence over SLOT 4.0.
            alternatives_for _${PN} ${SLOT} 5 "${arch_independent_alternatives[@]}"
        else
            alternatives_for _${PN} ${SLOT} ${SLOT} "${arch_independent_alternatives[@]}"
        fi
    fi

    if ever at_least 2.2.0 ; then
        # Upstream decided to downgrade SONAME back to 0 so we raise importance so SLOT 0 will
        # take precedence over SLOT 4.0.
        alternatives_for _$(exhost --target)_${PN} ${SLOT} 5 "${arch_dependent_alternatives[@]}"
    else
        alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    fi
}

