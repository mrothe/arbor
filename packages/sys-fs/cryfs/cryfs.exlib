# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ suffix='tar.gz' release=${PV} ] cmake

export_exlib_phases src_test

SUMMARY="Cryptographic filesystem for the cloud"
DESCRIPTION="
CryFS encrypts your files, so you can safely store them anywhere. It works
well together with cloud services like Dropbox, iCloud, OneDrive and others.
"
HOMEPAGE+=" https://www.cryfs.org/"

LICENCES="
    LGPL-3
    BSD-3         [[ note = [ Bundled copy of googletest ] ]]
    MIT           [[ note = [ Bundled copy of spdlog ] ]]
    public-domain [[ note = [ Bundled copy of crypto++ ] ]]
    "
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*
    build+run:
        dev-libs/boost[>=1.65.1]
        net-misc/curl
        sys-fs/fuse:0
        sys-libs/libgomp:=
"
# NOTE: Bundles dev-libs/crypto++, dev-libs/spdlog and dev-cpp/gtest

CMAKE_SOURCE=${WORKBASE}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Upstream only installs stuff with CMAKE_BUILD_TYPE=Release, so we hide
    # the default and set it accordingly.
    --hates=CMAKE_BUILD_TYPE
    -DCMAKE_BUILD_TYPE=Release
    -DBoost_USE_STATIC_LIBS:BOOL=FALSE
    -DCRYFS_UPDATE_CHECKS:BOOL=FALSE
    -DUSE_WERROR:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

cryfs_src_test() {
    # Upstream really doesn't provide a test target, the following is stolen
    # from .travisci/build_and_test.sh
    edo pushd "${WORK}"/test
    edo gitversion/gitversion-test
    edo cpp-utils/cpp-utils-test
    edo parallelaccessstore/parallelaccessstore-test
    edo blockstore/blockstore-test
    edo blobstore/blobstore-test
    edo cryfs/cryfs-test
    edo popd
}

